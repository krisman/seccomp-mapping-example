#include <stdio.h>
#include <signal.h>
#include <linux/seccomp.h>
#include <linux/filter.h>
#include <linux/audit.h>
#include <sys/ptrace.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <err.h>
#include <string.h>
#include <sys/mman.h>


#define SECCOMP_MODE_MAPPING	3 /* uses user-supplied filter. */
#define PROT_NOSYSCALL 0x40



typedef unsigned long (*syscall_fn_t)(long number,
				      unsigned long arg1, unsigned long arg2,
				      unsigned long arg3, unsigned long arg4,
				      unsigned long arg5, unsigned long arg6);
extern unsigned long
seccomp_syscall(long number,
		unsigned long arg1, unsigned long arg2,
		unsigned long arg3, unsigned long arg4,
		unsigned long arg5, unsigned long arg6);

void handle_sigsys(int i)
{
	printf("kernel intercepted something got %d.\n", i);

}


int main ()
{
	char *str_main = "Hello world from this binary fails\n";
	char *str_mmap = "Hello world from mmap should have failed\n";
	char *str_libc = "Hello World from libc works\n";
	int r;
	syscall_fn_t syscall_fn = NULL;

	if(signal(SIGSYS, handle_sigsys) < 0)
		err(1, "failed signal register");

	if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_MAPPING, 0) < 0)
		err(1, "failed mode mapping");

	syscall_fn = mmap(NULL, 4096, PROT_WRITE|PROT_EXEC|PROT_NOSYSCALL,
			  MAP_PRIVATE|MAP_ANON, -1, 0);

	if (syscall_fn == MAP_FAILED)
		err(1, "mmap failed");

	memcpy (syscall_fn, seccomp_syscall, 50);

	if (mprotect((void*)((unsigned long) main &  ~0xfff), 4096,
		     PROT_READ | PROT_EXEC | PROT_NOSYSCALL) < 0)
		err(1, "failed mode mapping");

	/* Try to issue syscall from a mapping blocked by mmap */
	r = syscall_fn(SYS_write, 1, (long) str_mmap, strlen(str_mmap), 0, 0, 0);

	/* Try to issue syscall from a mapping blocked by mprotect */
	r = seccomp_syscall(SYS_write, 1, (long) str_main, strlen(str_main), 0, 0, 0);

	/* Try to issue syscall from a mapping that is not blocked (libc) */
	write(1, str_libc, strlen(str_libc));

	return 0;
}
